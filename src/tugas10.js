import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <div className="judul" style={{"margin-left": "90px", "margin-top": "20px"}}>
        <h1>Form Pembelian Buah</h1>
      </div>
      <form id="simpleForm">
            <div>
                <label><b>Nama Pelanggan</b></label>
                <input type="text">
            </div>
            <br/>
            <span id="item"><b>Daftar Item</b></span> 
            <div id="checkbox-group" style="margin-left: 85px">
                <input type="checkbox" name="item">Semangka<br>
                <input type="checkbox" name="item">Jeruk<br>
                <input type="checkbox" name="item">Nanas<br>
                <input type="checkbox" name="item">Salak<br>
                <input type="checkbox" name="item">Anggur
            </div>
            <br/>
            <br/>
            <input class="submit-btn" type="button" value="Kirim">
      </form>
    </div>
  );
}

export default App;



class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      value: ''
    }
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <b>Nama Pelanggan</b> <input value={this.state.value} onChange={this.handleChange}/>
      </form>
    );
  }
}

export default App;