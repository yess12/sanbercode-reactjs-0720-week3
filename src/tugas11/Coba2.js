import React from 'react'

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
]
export default class Table extends React.Component {

    constructor(props) {
        super(props);
        this.getHeader = this.getHeader.bind(this);
        this.getRowsData = this.getRowsData.bind(this);
        this.getKeys = this.getKeys.bind(this);
    }

    getKeys = function() {
        return (
            Object.keys(this.props.dataHargaBuah[0])
        )
    }

    getHeader = function() {
        varKeys = this.getKeys();
        return keys.map((key, index) => {
            return (
                <th key={key}>
                    {key.charAt(0).toUpperCase()+key.slice(1)}
                </th>
            )
        })
    }

    getRowsData = function() {
        var items = this.props.dataHargaBuah;
        var keys = this.getKeys();
        return items.map((row, index) => {
            return (
                <tr key={index}>
                    <RenderRow key ={index} data={row} keys={keys}/>
                </tr>
            )
        })
    }

    render() {
        return (
            <div>
                <table>
                    <thead>
                        <tr>
                            {this.getHeader()}
                        </tr>
                    </thead>
                    <tbody>
                        {this.getRowsData()}
                    </tbody>
                </table>
            </div>
        );
    }
}

const RenderRow = props => {
    return props.keys.map((key, index) => {
        return (
            <td key = {props.data[key]}>
                {props.data[key]}
            </td>
        )
    })
}