import React from 'react';
import './Tugas11.css';

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
]

class Table extends React.Component {
    constructor(props) {
        super(props)
        this.state = {dataHargaBuah}
    }

    render() {
        return (
            <div>
                <h1>Tabel Harga Buah</h1>
            </div>
        )
    }

    renderTableData() {
        return this.state.dataHargaBuah.map((dataHargaBuah, index) => {
            const {nama, harga, berat} = dataHargaBuah
            return (
                <tr key={nama}>
                    <td>{nama}</td>
                    <td>{harga}</td>
                    <td>{berat/1000 + " kg"}</td>
                </tr>
            )
        })
    }

    render() {
        return (
            <div>
                <h1 id='title'>Tabel Harga Buah</h1>
                <table id='dataHargaBuah'>
                    <tbody>
                        {this.renderTableData()}
                    </tbody>
                </table>
            </div>
        )
    }

    renderTableHeader() {
        let header = Object.keys(this.state.dataHargaBuah[0])
        return header.map((key, index) => {
            return <th key={index}>{key.charAt(0).toUpperCase()+key.slice(1)}</th>
        })
    }

    render() {
        return (
            <div>
                <h1 id='title'>Tabel Harga Buah</h1>
                <table id='dataHargaBuah'>
                    <tbody>
                        <tr>{this.renderTableHeader()}</tr>
                        {this.renderTableData()}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Table

