import React, {Component} from 'react'
import Clock from './Tugas'

const styles = {
    container: {
        textAlign: 'center'
    }
}
class Timer extends Component{
    constructor(props){
        super(props)
        this.state = {
            time: 101
        }
    }

    componentDidMount(){
        if (this.props.start !== undefined) {
            this.setState({time: this.props.start})
        }
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentDidUpdate() {
        if(this.state.time < 0) {
            this.componentWillUnmount();
        }
    }

    componentWillUnmount(){
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            time: this.state.time - 1 
        });
    }


    render() {
        return(
        <>
            {this.state.time >= 0 &&
                <div style={{
                    margin: "0",
                    padding: "0",
                    display: "flex",
                    textAlign: "center",
                    display: "inline",
                    fontSize: "12px"
                }}>
                    <h1 style={{float: "left", marginLeft: "380px"}}>
                        sekarang jam : <Clock />
                    </h1>
                    <h1 style={{float: "right", marginRight: "380px"}}>
                        hitung mundur: {this.state.time}
                    </h1>
                </div>
            }
        </>
        )
    }
}

export default Timer