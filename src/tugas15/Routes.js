import React, {Component} from "react";
import { Switch, Link, Route } from "react-router-dom";

import DaftarBuah from '../tugas11/DaftarBuah';
import Timer from '../tugas12/Tugas12';
import Tabel from '../tugas13/Tugas13';
import Tabel2 from '../tugas14/Tugas14';
import Buah from '../tugas15/Tugas15';
import App from './theme'
import ReactDOM from "react-dom";

const Routes = () => {

  return (
    <>
    <div id ="root">
        <nav>
            <ul>
            <li>
                <Link to="/">Home</Link>
            </li>
            <li>
                <Link to="/tugas11">tugas11</Link>
            </li>
            <li>
                <Link to="/tugas12">tugas12</Link>
            </li>
            <li>
                <Link to="/tugas13">tugas13</Link>
            </li>
            <li>
                <Link to="/tugas14">tugas14</Link>
            </li>
            <li>
                <Link to="/tugas15">tugas15</Link>
            </li>
            </ul>
        </nav>
        <Switch>
            <Route path="/tugas11">
                <DaftarBuah />
            </Route>
            <Route path="/tugas12" component={Timer}/>
            <Route path="/tugas13">
            <Tabel />
            </Route>
            <Route path="/tugas14">
            <Tabel2 />
            </Route>
            <Route path="/tugas15">
            <Buah />
            </Route>
            <Route path="/" />
        </Switch>
    </div>
    </>
  );
};

export default Routes;